using DataFrames
using Gadfly
using RDatasets
#Variables de enttrada
#tasa llegadas o Landa
llegadas = 10.
inicioJornada = 8.0
media = 20.0
desviciaonEstandar = 0.3
exponecial = 2.0

#capacidad
capacidadServicio = 12.0

#datos
nroClientes = 10
aletoriosUno = rand(nroClientes)
tEntreLlegada = []
momentosLlegada = []
tiempoEsperaUno = []
tiempoEsperaDos = []

aletoriosDos = rand(nroClientes)
tiempoServicioUno = []
momentoTerminaServicioUno = []

aletoriosTres = rand(nroClientes)
tiempoServicioDos = []
momentoTerminaServicioDos = []

#tiempo entre llegadas
function tLlegadas(nroAleUno,cLlegadas)
    tEntreLlegadas::Real = (-((log(1-nroAleUno)/cLlegadas)))*60
    return tEntreLlegadas
end
function tServicioUno(nroAleUno,cLlegadas,nroAleDos)
    tEntreLlegadas::Real = 0
    if(nroAleUno < 0.5)
        tEntreLlegadas = (-((log(1-nroAleUno)/cLlegadas)))*60
    end
    return tEntreLlegadas
end
#tiempo de servico
function tServicioDos(nroAleUno, cServicio, nroAleTres)
    tEntreServicio::Real = 0
    if(nroAleUno >= 0.5)
        tEntreServicio = (-((log(1-nroAleTres)/cServicio)))*60
    end
    return tEntreServicio
end

#function de tiempo de espera Uno
function tEsperaUnoMax(nroAleUno, mLlegada, mTermServUno)
    tEUnoMax::Real = 0
    if(nroAleUno < 0.5)
        tEUnoMax = (max(mLlegada,mTermServUno))- mTermServUno
    end
    return tEUnoMax
end
#function de tiempo de espera Dos
function tEsperaDosMax(nroAleDos,mLlegada,mTermServUno)
    tEUnoMax::Real = 0
    if(nroAleDos >= 0.5)
        tEUnoMax = max(mLlegada,mTermServUno)-mLlegada
    end
    return tEUnoMax
end
#function de momento ternina se servicion uno
function mTerServUno(nroAleUno,mLlegada,tEspUno,tServUno,mServUno)
    mTServUno::Real = 0
    if(nroAleUno < 0.5)
        mTServUno = mLlegada+tEspUno+tServUno
    else
        mTServUno = mTServUno
    end
    return mTServUno
end
#function de momento ternina se servicion uno
function mTerServDos(nroAleUno,mLlegada,tEspDos,tServDos,mServDos)
    mTServDos::Real = 0.0
    if(nroAleUno >= 0.5)
        mTServDos = mLlegada+tEspDos+tServDos
    else
        mTServDos = mServDos
    end
    return mTServDos
end
#iteracion
x = 1
while x <= nroClientes
    tLleg = tLlegadas(aletoriosUno[x],llegadas)
    tEspServUno = tServicioUno(aletoriosUno[x],llegadas,aletoriosDos[x])
    tEspServDos = tServicioDos(aletoriosUno[x],llegadas,aletoriosTres[x])
    #tServ = tServicio(aletoriosDos[x],capacidadServicio)
    if x == 1
        #cliente
        #aleatorio uno
        append!(tEntreLlegada,round(tLleg,2))
        append!(momentosLlegada, round(tEntreLlegada[x] + inicioJornada))
        append!(tiempoEsperaUno, 0)
        append!(tiempoEsperaDos, 0)

        #aleatorio dos
        append!(tiempoServicioUno, round(tEspServUno,2))
        append!(momentoTerminaServicioUno, round(momentosLlegada[x]+tiempoEsperaDos[x],2))
        #aleatorio tres
        append!(tiempoServicioDos, round(tEspServDos,2))
        append!(momentoTerminaServicioDos, round(momentosLlegada[x]+tiempoEsperaDos[x]+tiempoServicioDos[x],2))

    else
        #cliente
        #aleatorio uno
        append!(tEntreLlegada,round(tLleg,2))
        append!(momentosLlegada, round(momentosLlegada[x-1] + tEntreLlegada[x],2))
        append!(tiempoEsperaUno, round(tEsperaUnoMax(aletoriosUno[x], momentosLlegada[x], momentoTerminaServicioUno[x-1]),2))
        append!(tiempoEsperaDos, round(tEsperaDosMax(aletoriosDos[x], momentosLlegada[x], momentoTerminaServicioDos[x-1]),2))

        #aleatorio dos
        append!(tiempoServicioUno, round(tEspServUno,2))
        append!(momentoTerminaServicioUno, round(mTerServUno(aletoriosDos[x],momentosLlegada[x],tiempoEsperaUno[x],tiempoServicioUno[x],momentoTerminaServicioUno[x-1]),2))
        ##aleatorio tres
        append!(tiempoServicioDos, round(tEspServDos,2))
        append!(momentoTerminaServicioDos, round(mTerServDos(aletoriosDos[x],momentosLlegada[x],tiempoEsperaDos[x],tiempoServicioDos[x],momentoTerminaServicioDos[x-1]),2))
    end
    x+=1
end

#Redendeo usar-->round(aletoriosUno[c],4)

#Mostrar en un tabla
DataFrame(AleatorioUno = aletoriosUno, TiempoLlegadas = tEntreLlegada, MLlegada = momentosLlegada, tEsperaUno = tiempoEsperaUno,
tEsperaDos = tiempoEsperaDos, AleatorioDos = aletoriosDos, tServicioUno = tiempoServicioUno, mTerServUno = momentoTerminaServicioUno,
aletoriosTres = aletoriosTres, tServicioDos = tiempoServicioDos, mTerServDos = momentoTerminaServicioDos)

#plot(x = tiempoInicio, y = tiempoServicio,Geom.point, Geom.line)
#plot(x = tiempoServicio, y = tiempoTerminaServicio,Geom.histogram)
