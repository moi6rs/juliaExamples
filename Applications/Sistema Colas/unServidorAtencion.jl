using DataFrames
using Gadfly
using RDatasets
#Variables de enttrada
#tasa llegadas o Landa
llegadas = 10.
println("Tasa LLegadas: ",llegadas)
#capacidad
capacidadServicio = 2
println("Capacidad Servicio: ", capacidadServicio)

#clientes
nroClientes = 100
println("Numeros de Clientes: ", nroClientes)

#numero Aleatorios para llegadas
aletoriosUno = rand(nroClientes)

tEntreLlegada = []
momentosLlegada = []
tiempoInicio = []
tiempoEspera = []

#aleatorio para servicio
aletoriosDos = rand(nroClientes)

tiempoServicio = []
tiempoTerminaServicio = []

#numero Aleatorios para llegadas
aletoriosTres = rand(nroClientes)
#Uniforme
maxB = 10
minA = 2
ventaEntradas = []

#tiempo entre llegadas
function tLlegadas(nroAleUno,cLlegadas)
    tEntreLlegadas::Real = (-((log(1-nroAleUno)/cLlegadas)))*60
    return tEntreLlegadas
end
#tiempo de servico
function tServicio(nroAleDos,cServicio)
    tEntreServicio::Real = (-((log(1-nroAleDos)/cServicio)))*60
    return tEntreServicio
end
#venta entradas 
function nroVentaEntradas(nroAleTres)
    tEntreServicio::Real = minA + ((maxB-minA)* nroAleTres)
    return tEntreServicio
end
#iteracion
x = 1
while x <= nroClientes
    tLleg = tLlegadas(aletoriosUno[x],llegadas)
    tServ = tServicio(aletoriosDos[x],capacidadServicio)
    nroVentas = nroVentaEntradas(aletoriosTres[x])
    if x == 1
        append!(tEntreLlegada,round(tLleg,2))
        append!(momentosLlegada,round(tEntreLlegada[x],2))
        append!(tiempoInicio,round(momentosLlegada[x],2))
        append!(tiempoEspera,round(tiempoInicio[x]-momentosLlegada[x],2))
        append!(tiempoServicio,round(tServ,2))
        append!(tiempoTerminaServicio,round(tiempoInicio[x]+tiempoServicio[x],2))
        append!(ventaEntradas,round(nroVentas))
    else
        append!(tEntreLlegada,round(tLleg,2))
        append!(momentosLlegada,round(momentosLlegada[x-1]+tEntreLlegada[x],2))
        append!(tiempoInicio,round(max(momentosLlegada[x],tiempoTerminaServicio[x-1]),2))
        append!(tiempoEspera,round(tiempoInicio[x]-momentosLlegada[x],2))
        append!(tiempoServicio,round(tServ,2))
        append!(tiempoTerminaServicio,round(tiempoInicio[x]+tiempoServicio[x],2))
        append!(ventaEntradas,round(nroVentas))
    end
    x+=1
end
#println(aletoriosUno)
#println(tEntreLlegada)
#println(momentosLlegada)
#println(tiempoInicio)
#println(tiempoEspera)
#println(aletoriosDos)
#println(tiempoServicio)
#println(tiempoTerminaServicio)
#Redendeo usar-->round(aletoriosUno[c],4)

#Mostrar en un tabla
DataFrame(AleaUno = aletoriosUno, TEntreLleg = tEntreLlegada, 
        MomentoLleg= momentosLlegada, TInicio = tiempoInicio,
        TEspera = tiempoEspera, AleaDos = aletoriosDos,
        TServicio = tiempoServicio, TFin = tiempoTerminaServicio,
        AleaTres = aletoriosTres, EntVendidas = ventaEntradas)

#Graficos
#cantidadEntradas = [1,2,3,4,5,6,7,8,9,10,11]
#plot(x = tEntreLlegada, y = tiempoServicio,Geom.point, Geom.line)
#plot(x = ventaEntradas, y = cantidadEntradas,Geom.histogram)